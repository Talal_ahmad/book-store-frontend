# Angular Frontend Setup and Run Instructions

Welcome to the Angular frontend of my project! This guide will help you set up the development environment and run the application locally.

## Prerequisites

Before you begin, ensure you have the following installed on your development machine:

- Node.js (version 14 LTS or higher)
- npm (Node Package Manager)
- Angular CLI

You can install Angular CLI globally using npm:

```bash
npm install -g @angular/cli
```

# Books Library

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 18.0.7.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.dev/tools/cli) page.
