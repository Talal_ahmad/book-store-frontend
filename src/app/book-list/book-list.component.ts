import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { BookService } from '../book.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import $ from 'jquery';
import Toastify from 'toastify-js';

export interface Book {
  id: string;
  title: string;
  author: string;
  publication_year: number;
  created_at: string;
  updated_at: string;
}

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css'],
})
export class BookListComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = [
    'id',
    'title',
    'author',
    'publication_year',
    'created_at',
    'updated_at',
    'actions',
  ];
  dataSource = new MatTableDataSource<Book>([]);

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private bookService: BookService, private router: Router) {}

  ngOnInit(): void {
    this.getbooks();
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue : any) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openUpdateForm(id: string): void {
    this.router.navigate(['books/edit', id]);
  }

  getbooks(): void {
    this.bookService.getBooks().subscribe((data: Book[]) => {
      this.dataSource.data = data;
    });
  }

  insertbooks(): void {
    $('.insert_books_btn').prop('disabled', true);
    this.bookService.insertbooks().subscribe(() => {
      Toastify({
        text: 'Books Added Successfully',
        duration: 1500,
        close: true,
        gravity: 'top',
        position: 'right',
        stopOnFocus: true,
        style: {
          background: 'linear-gradient(to right, #51A351, #00b09b)',
        },
        className: 'custom_toast', /* Manually added because className prop is not working in node_modules/toastify-js/src/toastify.css */
      }).showToast();
      $('.insert_books_btn').prop('disabled', false);
      this.getbooks();
    });
  }

  deleteBook(id: string): void {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.bookService.deleteBook(id).subscribe(() => {
          Swal.fire({
            title: 'Deleted!',
            text: 'Your book has been deleted.',
            icon: 'success',
          });
          this.dataSource.data = this.dataSource.data.filter(
            (book) => book.id !== id
          );
        });
      }
    });
  }
}
