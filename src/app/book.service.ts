import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BookService {
  private apiUrl = 'http://localhost/slim-api/books_api/books_library';

  constructor(private http: HttpClient) {}

  getBooks(): Observable<any> {
    return this.http.get(this.apiUrl);
  }

  insertbooks(): Observable<any> {
    return this.http.get(`${this.apiUrl}/insertbooks`);
  }

  getBookById(id: number): Observable<any> {
    return this.http.get(`${this.apiUrl}/${id}`);
  }

  createBook(book: any): Observable<any> {
    const body = new HttpParams()
      .set('title', book.title)
      .set('author', book.author)
      .set('publication_year', book.publication_year);

    // Set headers for URL-encoded data
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    });

    return this.http.post<any>(this.apiUrl, body.toString(), { headers });
  }

  updateBook(id: number, book: any): Observable<any> {
    // alert(id);
    const body = new HttpParams()
      .set('title', book.title)
      .set('author', book.author)
      .set('publication_year', book.publication_year);

    // Set headers for URL-encoded data
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
    });

    return this.http.post<any>(`${this.apiUrl}/${id}`, body.toString(), {
      headers,
    });
  }

  deleteBook(id: string): Observable<any> {
    return this.http.delete(`${this.apiUrl}/delete_book/${id}`);
  }
}
