import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BookService } from '../book.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-book-create',
  templateUrl: './book-create.component.html',
  styleUrls: ['./book-create.component.css'],
})
export class BookCreateComponent {
  bookForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private bookService: BookService,
    private router: Router
  ) {
    this.bookForm = this.fb.group({
      title: ['', Validators.required],
      author: ['', Validators.required],
      publication_year: [
        '',
        [Validators.required, Validators.pattern('^[0-9]{4}$')],
      ],
    });
  }

  createBook(): void {
    if (this.bookForm.valid) {
      this.bookService.createBook(this.bookForm.value).subscribe(
        () => {
          Swal.fire(
            'Book Created',
            'The book has been successfully added!',
            'success'
          );
          this.router.navigate(['/books']);
        },
        (error) => {
          let errorMessage = '';
          try {
            const errors = JSON.parse(error.error.exception[0].message);
            errorMessage = this.formatErrors(errors);
          } catch (e) {
            errorMessage = error.error.exception[0].message;
          }
          Swal.fire('Error', errorMessage, 'error');
        }
      );
    } else {
      Swal.fire('Error', 'Please fill out the form correctly.', 'error');
    }
  }

  formatErrors(errors: any): string {
    let errorMessage = '';
    for (const [key, messages] of Object.entries(errors)) {
      errorMessage += `<strong>${
        key.charAt(0).toUpperCase() + key.slice(1)
      }:</strong><br>`;
      for (const message of messages as string[]) {
        errorMessage += `- ${message}<br>`;
      }
      errorMessage += '<br>';
    }
    return errorMessage;
  }

  get title() {
    return this.bookForm.get('title');
  }

  get author() {
    return this.bookForm.get('author');
  }

  get publicationYear() {
    return this.bookForm.get('publication_year');
  }
}
