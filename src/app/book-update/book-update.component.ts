import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BookService } from '../book.service';
import Swal from 'sweetalert2';
import Toastify from 'toastify-js';

@Component({
  selector: 'app-book-update',
  templateUrl: './book-update.component.html',
  styleUrls: ['./book-update.component.css'],
})
export class BookUpdateComponent implements OnInit {
  bookForm: FormGroup;
  bookId: number = 0;

  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private bookService: BookService,
    private router: Router
  ) {
    this.bookForm = this.fb.group({
      title: ['', Validators.required],
      author: ['', Validators.required],
      publication_year: [
        '',
        [Validators.required, Validators.pattern('^[0-9]{4}$')],
      ],
    });
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.bookId = Number(id);
    this.bookService.getBookById(this.bookId).subscribe(
      (data) => {
        if (Array.isArray(data) && data.length > 0) {
          const bookData = data[0];
          this.bookForm.patchValue({
            title: bookData.title,
            author: bookData.author,
            publication_year: bookData.publication_year,
          });
        } else {
          console.error('Invalid API response format');
        }
      },
      (error) => {
        Toastify({
          text: error.error.error,
          duration: 4000,
          close: true,
          gravity: 'top',
          position: 'right',
          stopOnFocus: true,
          style: {
            background: 'linear-gradient(to right, #51A351, #00b09b)',
          },
          className:'custom_toast' /* Manually added because className prop is not working in node_modules/toastify-js/src/toastify.css */,
          callback: () => {
            this.router.navigate(['/books']);
          },
        }).showToast();
      }
    );
  }

  updateBook(): void {
    if (this.bookForm.valid) {
      this.bookService.updateBook(this.bookId, this.bookForm.value).subscribe(
        () => {
          Swal.fire(
            'Book Updated',
            'The book has been successfully updated!',
            'success'
          );
          this.router.navigate(['/books']);
        },
        (error) => {
          let errorMessage = '';
          try {
            const errors = JSON.parse(error.error.exception[0].message);
            errorMessage = this.formatErrors(errors);
          } catch (e) {
            errorMessage = error.error.exception[0].message;
          }
          Swal.fire('Error', errorMessage, 'error');
        }
      );
    } else {
      Swal.fire('Error', 'Please fill out the form correctly.', 'error');
    }
  }

  formatErrors(errors: any): string {
    let errorMessage = '';
    for (const [key, messages] of Object.entries(errors)) {
      errorMessage += `<strong>${
        key.charAt(0).toUpperCase() + key.slice(1)
      }:</strong><br>`;
      for (const message of messages as string[]) {
        errorMessage += `- ${message}<br>`;
      }
      errorMessage += '<br>';
    }
    return errorMessage;
  }

  get title() {
    return this.bookForm.get('title');
  }

  get author() {
    return this.bookForm.get('author');
  }

  get publicationYear() {
    return this.bookForm.get('publication_year');
  }
}
